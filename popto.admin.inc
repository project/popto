<?php

/**
 * @file
 *  Functions for settings page, eventually other admin features.
 */
 
/**
 * FormAPI callback for module settings.
 */
function popto_admin_settings() {
  $form = array();
  
  $form['popto_loader_script'] = array(
    '#type' => 'textarea',
    '#title' => t('Pop.to Loader Script'),
    '#default_value' => variable_get('popto_loader_script', ''),
    '#required' => TRUE,
    '#description' => t('Paste in your "loader" script here. This is the one beginning with "<!-- Pop.to Widget Loader Script -->".'),
  );
  
  return system_settings_form($form);
}