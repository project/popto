
NOTE: requires jQuery 1.3 or higher. That means using the 6.x-2.dev version of jquery update!

To use, you set up your pop.to widgets, and then place the code in the field when you define it. You can then cleanly associate the widgets with your nodes.

Much more coming, but this initial implementation made it nice and clean to do for Mission bicycle!